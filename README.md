# JavaScript - Basic React exercises 

Here are some exercises I have made with React, these are very basic exercises. I haven't done much coding with REACT but I know a litle about it.

Exercise 1

 - This is basic sum calculator made with JavaScript and REACT. Application adds up numbers in fields 1 and 2. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset8/Harjoitus4.html
 
 Exercise 2
 
  - This is a little bit modified version from the first exercise. A calculator where you can choose the method what to use. This one is also made with JavaScript and REACT. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset8/Harjoitus5.html